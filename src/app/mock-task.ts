import { Task } from './Task'
export const TASKS: Task[] = [
    {
        id: 1,
        text: 'Doctors Appointment',
        day: 'May 5th at 2:30pm',
        reminder: true,
    },
       { 
        id: 2,
        text: 'Finish the article',
        day: 'May 6th at 5:30pm',
        reminder: true,
    },
        {
        id: 3,
        text: 'Work on the application',
        day: 'May 7th at 11:00am',
        reminder: false,
    },
     
]